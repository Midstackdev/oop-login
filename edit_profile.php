<?php include 'includes/header.php'; ?>

<?php include 'includes/navbar.php'; ?>

<?php 
    
    if(!$user->isLoggedIn()){
        Redirect::to('index.php');
    }

    if(Input::exists()){
        if(Token::check(Input::get('token'))){

            $validation = $validate->check($_POST, array(
                'name' => array(
                    'required' => true,
                    'min' => 2,
                    'max' =>50
                )
            ));

            if($validation->passed()){
                //update
                try{
                    $user->update(array(
                        'name' => Input::get('name')
                    ));

                    Session::flash('home', "Your details have been updated. ");
                    Redirect::to('index.php');
                    
                }catch(Exeption $e){
                    die($e->getMessage());
                }

            }else{
                foreach($validation->errors() as $error){
                    echo $error, '<br>';
                }
            }
        }
    }




?>

    <div class="container">

        <div class="row">

            <div class="col-md-6 mx-auto">
                <div class='card card-body  bg-light mt-5'>

                    <h2>Update Your account Details</h2>
                    <p>
                       

                    </p>
                    <form action="" method='POST'>
                        <div class="form-group">
                            <label for='name'>Name: <sup>*</sup></label>
                            <input type='text' name="name" class='form-control form-control-lg' 
                            value="<?php echo escape($user->data()->name); ?>">
                            <span class="invalid-feedback"></span>
                        </div>

                     
                        
                               
                        <div class="form-group">
                            <label for='username'>Image: <sup>*</sup></label>
                            <input type='file' name="image" class='form-control'>
                            <span class="invalid-feedback"></span>
                        </div>
                        
                        
                        <div class="row">

                            <div class='col'>

                                <input type='submit' name='edit' value='Update Now' class='btn color-set btn-block'>
                                <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">

                            </div>


                            <div class='col'>

                                <a href="change_password.php" class="btn btn-light btn-block">Wanna Change Password? </a>

                            </div>

                        </div>

                    </form>

                </div>
            </div>

        </div>


    </div>




<?php include 'includes/footer.php'; ?>