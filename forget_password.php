<?php include 'includes/header.php'; ?>

<?php include 'includes/navbar.php'; ?>

    <div class="container">


        <div class="row">

            <div class="col-md-6 mx-auto">
                <div class='card card-body  bg-light mt-5'>
                    <h2>Reset Password</h2>
                    <p>Please fill in credentials to get reset link.</p>
                    
                    <form action="" method='POST'>


                       <div class="form-group">
                            <label for='email'>Email: <sup>*</sup></label>
                            <input type='email' name="email" class='form-control form-control-lg'>
                            <span class="invalid-feedback"></span>
                        </div>
                       


                        <div class="row">

                            <div class='col'>

                                <input type='submit' name='reset_password' value='Send Reset Link' class='btn  btn-block color-set'>

                            </div>



                        </div>
                        <div class="row">
                            <div class='col'>

                                <a href="" class="btn text-right  btn-block">Go Back to Login </a>

                            </div>
                       
                        </div>


                    </form>

                </div>
            </div>

        </div>


    </div>










<?php include 'includes/footer.php'; ?>
