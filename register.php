<?php include 'includes/header.php'; ?>

<?php include 'includes/navbar.php'; ?>

<?php 
    // require_once 'core/init.php';



    if(Input::exists()){
        if(Token::check(Input::get('token'))){
            // $validate = new Validate();
            $validation = $validate->check($_POST, array(
                'username' => array(
                    'required' => true,
                    'min' => 2,
                    'max' => 20,
                    'unique' => 'users'
                ),
                'password' => array(
                    'required' => true,
                    'min' => 6
                ),
                'confirm_password' => array(
                    'required' => true,
                    'matches' => 'password'
                ),
                'name' => array(
                    'required' => true,
                    'min' => 2,
                    'max' => 50
                )

            ));

            if($validation->passed()){
                //register user
                // $user = new User();

                $salt = Hash::salt(32);

                try{
                    $user->create(array(
                        'username' => Input::get('username'),
                        'password' => Hash::make(Input::get('password'), $salt),
                        'salt' => $salt,
                        'name' => Input::get('name'),
                        'joined' => date('Y-m-d H:i:s'),
                        'group' => 1 

                    ));

                    Session::flash('home', 'You have been registered and can now log in!');
                    Redirect::to('index.php');


                }catch(Exception $e){
                    die($e->getMessage());
                }
            }else{
                //output errors
                // print_r($validation->errors());
                foreach($validation->errors() as $error){
                    echo $error, '<br>';
                }
            }
        } 
    }

?>

    <div class="container">


        <div class="row">

            <div class="col-md-6 mx-auto">
                <div class='card card-body  bg-light mt-2 mb-5'>
                    <h2>Register</h2>
                    <p>Please fill in credentials to Sign Up.</p>
                    <form action="" method='POST'>

                        <div class="form-group">
                            <label for='name'>Name: <sup>*</sup></label>
                            <input type='name' name="name" class='form-control form-control-lg' value="<?php echo escape(Input::get('name')) ?>">
                            <span class="invalid-feedback"></span>
                        </div>
                        
                        <div class="form-group">
                            <label for='username'>Username: <sup>*</sup></label>
                            <input type='text' name="username" class='form-control form-control-lg' value="<?php echo escape(Input::get('username')) ?>">
                            <span class="invalid-feedback"></span>
                        </div>


                        <div class="form-group">
                            <label for='email'>Email: <sup>*</sup></label>
                            <input type='email' name="email" class='form-control form-control-lg'>
                            <span class="invalid-feedback"></span>
                        </div>

                        <div class="form-group">
                            <label for='password'>Password: <sup>*</sup></label>
                            <input type='password' name="password" class='form-control form-control-lg'>
                            <span class="invalid-feedback"></span>
                        </div>
                        
                        <div class="form-group">
                            <label for='confirm_password'>Confirm Password: <sup>*</sup></label>
                            <input type='password' name="confirm_password" class='form-control form-control-lg'>
                            <span class="invalid-feedback"></span>
                        </div>


                        <div class="row">

                            <div class='col'>

                                <input type='submit' name='register' value='Register' class='btn  btn-block color-set'>

                                <input type='hidden' name='token' value="<?php echo Token::generate(); ?>">

                            </div>



                        </div>
                        <div class="row">
                            <div class='col'>

                                <a href="login.php" class="btn  btn-block">Have account? Login</a>

                            </div>
                        </div>


                    </form>

                </div>
            </div>

        </div>


    </div>





<?php include 'includes/footer.php'; ?>
