<?php include 'includes/header.php'; ?>

<?php include 'includes/navbar.php'; ?>

<?php
    if(!$user->isLoggedIn()){
        Redirect::to('index.php');
    }

    if(!$username = Input::get('user')){
        Redirect::to('index.php');
    }else{
        $user = new User($username);
        if(!$user->exists()){
            Redirect::to(404);
        }else{
            $data = $user->data();
            if(Session::get(Config::get('session/session_name')) !== $data->id){
                Redirect::to(404);
            }
        }
    }

?>

    <div class="container">

        <div class='jumbotron jumbotron-fluid text-center color-set'>
            <div class="container">
                <h1 class='display-3'>
                    Profile Managment
                </h1>
                <p class='lead'>
                    Here you will be able to upload image and edit information
                </p>

            </div>
        </div>

        <div class="col-md-6 mx-auto">

            <div class='card'>


                <div class="card-header color-set">

                    Your Profile Data
                </div>
                <div class='card-body '>
                   

                    <div class="row">

                        <div class="col-md-8">
                            <div class='detail-text'>
                                <label for="name"><strong>Name:</strong></label>
                                <span class='text-data'> <?php echo escape($data->name); ?></span>
                            </div>

                            <div class='detail-text'>
                                <label for="name"><strong>Email:</strong></label>
                                <span class='text-data'> <?php echo escape($data->username); ?></span>
                            </div>

                            <div class='detail-text'>
                                <label for="name"><strong>Account Status:</strong></label>
                                <span class='text-data'> Verified</span>
                            </div>

                            <hr/>
                            <div class='detail-text'>
                                <label for="name"><strong>Created at:</strong></label>
                                <span class='text-data'><?php echo escape($data->joined); ?></span>
                            </div>

                        </div>
                        <div class="col-md-4">

                            <a href="https://placeholder.com"><img src="http://via.placeholder.com/150x150"></a>

                        </div>
                    </div>

                </div>
                <div class='card-footer'>
                    <a href='' data-toggle="modal" data-target="#myModal"><i class='fa fa-trash-o'></i></a>
                    <a href="edit_profile.php" class='pull-right'><i class='fa fa-pencil-square-o'></i></a>
                </div>

            </div>
        </div>

        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" style='cursor:pointer;'>&times;</button>

                    </div>
                    <div class="modal-body text-center">
                        <p>Do you really want to delete your account?</p>
                    </div>
                    <div class="modal-footer">
                        <a href="" class="btn btn-danger">Yes</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal" style='cursor:pointer;'>No</button>
                    </div>
                </div>

            </div>
        </div>



    </div>



<?php include 'includes/footer.php'; ?>
