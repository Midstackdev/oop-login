<?php include 'includes/header.php'; ?>

<?php include 'includes/navbar.php'; ?>

  


<div class="container">


    <div class="row">

        <div class="col-md-6 mx-auto mt-5">
            <div class='card card-body  bg-light '>
                <h2>Activate Account Request</h2>
             
               
                <form action="" method='POST'>


                     <div class="form-group">
                        <label for='email'>Email: <sup>*</sup></label>
                        <input type='email' name="email" value="" class="form-control form-control-lg">
                         <span class="invalid-feedback"></span>
                    </div>
                    
                    
                    <div class="row">

                        <div class='col'>

                            <input type='submit' name='request-activate-account' value='Send Reset Link' class='btn  btn-block color-set'>

                        </div>



                    </div>
                    <div class="row">
                        <div class='col'>

                            <a href="#" class="btn text-right  btn-block">Go Back to Login </a>

                        </div>

                    </div>


                </form>

            </div>
        </div>

    </div>


</div>





<?php include 'includes/footer.php'; ?>
