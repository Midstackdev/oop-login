<?php include 'includes/header.php'; ?>

<?php include 'includes/navbar.php'; ?>

<?php 
    // require_once 'core/init.php';

    if(Input::exists()){
        if(Token::check(Input::get('token'))){

            // $validate = new Validate();
            $validation = $validate->check($_POST, array(
                'username' => array('required' => true),
                'password' => array('required' => true)
            ));

            if($validation->passed()){
                //log user in
                // $user = new User();
                $remember = (Input::get('remember') === 'on') ? true : false;
                $login = $user->login(Input::get('username'), Input::get('password'), $remember);

                if($login){
                   Redirect::to('index.php');
                }else{
                    echo 'Sorry, login failed';
                }
            }else{
                foreach($validation->errors() as $error){
                    echo $error, '<br>';
                }
            }
        }
    }
?>

    <div class="container">


        <div class="row">

            <div class="col-md-6 mx-auto">
                <div class='card card-body  bg-light mt-5'>
                    <h2>Login</h2>
                    <p>Please fill in credentials to log in.</p>
                    <form action="" method='POST'>


                        <div class="form-group">
                            <label for='email'>Username: <sup>*</sup></label>
                            <input type='text' name="username" class='form-control form-control-lg'>
                            <span class="invalid-feedback"></span>
                        </div>

                        <div class="form-group">
                            <label for='password'>Password: <sup>*</sup></label>
                            <input type='password' name="password" class='form-control form-control-lg' autocomplete="off">
                            <span class="invalid-feedback"></span>
                        </div>

                        <div class="form-check mb-2 text-center">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1" name="remember" autocomplete="off">
                            <label class="form-check-label text-primary" for="exampleCheck1">Remember Me</label>
                        </div>


                        <div class="row">

                            <div class='col'>
                                
                                <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
                                <input type='submit' name='login' value='Login' class='btn  btn-block color-set'>

                            </div>



                        </div>
                        <div class="row">
                            <div class='col'>

                                <a href="" class="btn  btn-block">Forget Passsword?</a>

                            </div>


                            <div class='col'>

                                <a href="register.php" class="btn  btn-block">No account? Register</a>

                            </div>
                        </div>


                    </form>

                </div>
            </div>

        </div>


    </div>



<?php include 'includes/footer.php'; ?>
