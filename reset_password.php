<?php include 'includes/header.php'; ?>

<?php include 'includes/navbar.php'; ?>

    <div class="container">


        <div class="row">

            <div class="col-md-6 mx-auto mt-5">
                <div class='card card-body  bg-light mt-2 mb-5'>
                    <h2>Reset your Password</h2>
                    <p>Please fill in credentials to Reset Password.</p>
                    <form action="" method='POST'>

                        <div class="form-group">
                            <label for='password'>Password: <sup>*</sup></label>
                            <input type='password' name="password" class='form-control form-control-lg'>
                            <span class="invalid-feedback"></span>
                        </div>
                        
                        <div class="form-group">
                            <label for='confirm_password'>Confirm Password: <sup>*</sup></label>
                            <input type='password' name="confirm_password" class='form-control form-control-lg'>
                            <span class="invalid-feedback"></span>
                        </div>


                        <div class="row">

                            <div class='col'>

                                <input type='submit' name='reset_password' value='Reset Password' class='btn  btn-block color-set'>

                            </div>



                        </div>
                        


                    </form>

                </div>
            </div>

        </div>


    </div>




<?php include 'includes/footer.php'; ?>
