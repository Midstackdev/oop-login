<?php include 'includes/header.php'; ?>

<?php include 'includes/navbar.php'; ?>

<?php 
    if(!$user->isLoggedIn()){
        Redirect::to('index.php');
    }

    if(Input::exists()){
        if(Token::check(Input::get('token'))){
            
            $validation = $validate->check($_POST, array(
                'old_password' => array(
                    'required' => true,
                    'min' => 6
                ),
                'password' => array(
                    'required' => true,
                    'min' => 6
                ),
                'confirm_password' => array(
                    'required' => true,
                    'min' => 6,
                    'matches' => 'password'
                )
            ));

            if($validation->passed()){
                //change password
                if(Hash::make(Input::get('old_password'), $user->data()->salt) !== $user->data()->password){
                    echo "Your current password is wrong";
                }else{
                    $salt = Hash::salt(32);
                    $user->update(array(
                        'password' => Hash::make(Input::get('password'), $salt),
                        'salt' => $salt
                    ));

                    Session::flash('home', 'Your password has been changed!');
                    Redirect::to('index.php');
                }

            }else{
                foreach($validation->errors() as $error){
                    echo $error, '<br>';
                }
            }
        }
    }

?>

    <div class="container">


        <div class="row">

            <div class="col-md-6 mx-auto mt-5">
                <div class='card card-body  bg-light mt-2 mb-5'>
                    <h2>Change your Password</h2>
                    <p>Please fill in credentials to Change Password.</p>
                    <form action="" method='POST'>
                        
                        <div class="form-group">
                            <label for='password'>Old Password: <sup>*</sup></label>
                            <input type='password' name="old_password" class='form-control form-control-lg'>
                            <span class="invalid-feedback"></span>
                        </div>

                        <div class="form-group">
                            <label for='password'>New Password: <sup>*</sup></label>
                            <input type='password' name="password" class='form-control form-control-lg'>
                            <span class="invalid-feedback"></span>
                        </div>
                        
                        <div class="form-group">
                            <label for='confirm_password'>Confirm Password: <sup>*</sup></label>
                            <input type='password' name="confirm_password" class='form-control form-control-lg'>
                            <span class="invalid-feedback"></span>
                        </div>


                        <div class="row">

                            <div class='col'>

                                <input type='submit' name='reset_password' value='Reset Password' class='btn  btn-block color-set'>
                                <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">

                            </div>



                        </div>
                        


                    </form>

                </div>
            </div>

        </div>


    </div>





<?php include 'includes/footer.php'; ?>
